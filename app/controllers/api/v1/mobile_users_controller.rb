module Api
  module V1
  	class MobileUsersController < Api::V1::BaseController

      def registerd_user?
        MobileUser.exists?(uuid: create_params[:uuid])
      end

      # register user through uuid and register id
      def register
        if registerd_user?
          render json: {
            message: 'user is already register',
            status: 'already_register',
            auth_key:
              current_mobile_user.present? ? current_mobile_user.auth_key : '',
            mobile_user_id: 
              current_mobile_user.present? ? current_mobile_user.id : ''
          }
        else
          new_user = MobileUser.new(create_params)
          new_user.auth_key = SecureRandom.base64(64)
          if new_user.save
            render json: {
              auth_key: new_user.auth_key,
              mobile_user_id: new_user.auth_key,
              message: 'user register succefully',
              status: 'register'
            }
          else
            render json: {
              message: 'user not register succefully',
              status: 'error'
            }
          end
        end
      end

      private

      def create_params
        params.permit(:uuid, :os_type)
      end

      def current_mobile_user
        MobileUser.find_by_uuid(create_params[:uuid])
      end
    end
  end
end