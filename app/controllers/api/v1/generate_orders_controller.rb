module Api
  module V1
    class GenerateOrdersController < Api::V1::BaseController
      # before_filter :authenticate_user!

      def alcohol_listing
        categories = Category.all
        listing = Api::V1::AlcoholList.new(categories)
        render json: { status: 200, data: listing.list }
      end

      def mixers_listing
        mixers = Category.where(title: 'MIXERS')
        listing = Api::V1::MixersList.new(mixers)
        render json: { status: 200, data: listing.list }
      end

      def place_order
        binding.pry
        @order = Order.new(order_params)
        if @order.save
          render json: { status: 200, message: 'success'}
        else
          render json: { status: 200, message: 'fail'}
        end
      end

      def fetch_orders
        render json: {status: 200, data: MobileUser.find(2).orders}
      end


      private

      def order_params
        params.require(:name)
        params.require(:product_id)
        params.require(:quantity)
        params.require(:table_number)
        params.require(:price)
        params.require(:mobile_user_id)
        params.permit(:name, :product_id, :quantity, :table_number, :price, :mixers, :mixers_quantity, :mobile_user_id)
      end
    end
  end
end
