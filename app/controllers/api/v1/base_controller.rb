module Api
  module V1
    class BaseController < ApplicationController
      # skip_before_filter :authenticate_user!
      rescue_from ActionController::ParameterMissing,
                  with: :respond_with_parameter_missing
      rescue_from ActiveRecord::RecordNotFound, 
                  with: :record_not_found_response

      def authenticate_user!
        @current_user = MobileUser.find_by_uuid_and_auth_key(
          current_uuid, current_auth_token
        )
        MobileUser.current = @current_user
        return unauthorized_user if @current_user.nil?
        @current_user
      end

      def respond_with_parameter_missing
        render json: { status: 400, message: 'param missing' }
      end

      def current_user
        @current_user
      end

      private

      def current_uuid
        request.headers[:HTTP_UUID] || request.headers[:uuid]
      end

      def current_auth_token
        request.headers[:HTTP_AUTH] || request.headers[:auth]
      end

      def not_found
        render json: {
          message: 'No Results Found,</br> Oh no! Please try again',
          status: :not_found
        }
      end

      def unauthorized_user
        render json: { message: 'User not authorized!', status: :unauthorized }
      end

    end
  end
end
