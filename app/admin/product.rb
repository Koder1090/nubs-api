ActiveAdmin.register Product do
  permit_params :name, :base_price, :high_price, :category_id

  index do
    selectable_column
    id_column
    column :name
    column :base_price
    column :high_price
    column :category
    actions
  end

  filter :name
  filter :base_price
  filter :high_price
  filter :category

  form do |f|
    f.inputs do
      f.input :name
      f.input :base_price
      f.input :high_price
      f.input :category
    end
    f.actions
  end

end
