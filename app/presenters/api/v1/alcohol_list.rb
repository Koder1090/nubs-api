module Api
  module V1
    class AlcoholList
      def initialize(categories)
        @categories = categories
      end

      def list
        Api::V1::AlcoholListSerializer.new(@categories)
      end
    end
  end
end
