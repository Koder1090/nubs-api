--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO postgres;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categories (
    id bigint NOT NULL,
    title character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.categories OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE products (
    id bigint NOT NULL,
    category_id bigint,
    name character varying,
    base_price integer,
    high_price integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.products OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2018-01-20 12:05:44.006771	2018-01-20 12:05:44.006771
\.


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY categories (id, title, created_at, updated_at) FROM stdin;
1	BEER	2018-01-20 12:05:49.680406	2018-01-20 12:05:49.680406
2	RUM	2018-01-20 12:06:21.950847	2018-01-20 12:06:21.950847
3	WHISKEY	2018-01-20 12:06:30.063943	2018-01-20 12:06:30.063943
4	SCOTCH	2018-01-20 12:06:47.063767	2018-01-20 12:06:47.063767
5	SINGLE MALT	2018-01-20 12:09:31.598505	2018-01-20 12:09:31.598505
6	GIN	2018-01-20 12:10:15.605994	2018-01-20 12:10:15.605994
7	TEQUILA	2018-01-20 12:10:23.869961	2018-01-20 12:10:23.869961
8	VODKA	2018-01-20 12:10:28.87	2018-01-20 12:10:28.87
9	COGNAC	2018-01-20 12:10:40.741897	2018-01-20 12:10:40.741897
10	COCKTAILS	2018-01-20 12:11:05.781674	2018-01-20 12:11:05.781674
11	SHOOTERS	2018-01-20 12:11:11.564963	2018-01-20 12:11:11.564963
12	WHITE WINE	2018-01-20 12:11:34.484747	2018-01-20 12:11:34.484747
13	RED WINE	2018-01-20 12:11:41.37265	2018-01-20 12:11:41.37265
14	ROSE WINE	2018-01-20 12:11:55.460774	2018-01-20 12:11:55.460774
15	SPARKLING WINE	2018-01-20 12:12:07.076631	2018-01-20 12:12:07.076631
18	MIXERS	2018-01-20 12:58:07.976836	2018-01-20 12:58:07.976836
19	Liquers	2018-01-20 14:04:56.86025	2018-01-20 14:04:56.86025
20	Breezers	2018-01-20 14:05:03.420144	2018-01-20 14:05:03.420144
\.


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categories_id_seq', 20, true);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products (id, category_id, name, base_price, high_price, created_at, updated_at) FROM stdin;
2	1	Kingfisher Ultra	\N	\N	2018-01-20 13:01:45.065669	2018-01-20 13:01:45.065669
1	1	Kingfisher Lager	\N	\N	2018-01-20 13:00:58.054311	2018-01-20 13:13:32.645513
3	1	Bira	\N	\N	2018-01-20 13:02:00.025522	2018-01-20 13:16:05.253282
4	1	Carlsberg	\N	\N	2018-01-20 13:02:14.232891	2018-01-20 13:16:48.368634
5	1	Hoegaarden	\N	\N	2018-01-20 13:02:59.136169	2018-01-20 13:18:55.355884
6	1	Corona	\N	\N	2018-01-20 13:03:09.840364	2018-01-20 13:20:24.466461
7	2	Old Monk	\N	\N	2018-01-20 13:05:02.809024	2018-01-20 13:22:00.177764
8	2	Bacardi White	\N	\N	2018-01-20 13:05:31.111589	2018-01-20 13:25:23.912464
9	2	Captain Morgan	\N	\N	2018-01-20 13:06:15.807427	2018-01-20 13:25:49.408072
10	2	Bacardi Black Rum	\N	\N	2018-01-20 13:07:13.942449	2018-01-20 13:26:17.07137
11	2	Old Monk Legend	\N	\N	2018-01-20 13:07:42.078675	2018-01-20 13:26:34.48741
14	3	Blenders Pride	\N	\N	2018-01-20 13:28:56.277436	2018-01-20 13:28:56.277436
15	3	Jim Beam	\N	\N	2018-01-20 13:30:01.837024	2018-01-20 13:30:01.837024
16	3	Jameson Irish	\N	\N	2018-01-20 13:30:12.580708	2018-01-20 13:30:12.580708
17	3	Jack Daniels	\N	\N	2018-01-20 13:30:23.868713	2018-01-20 13:30:23.868713
18	4	100 Pipers 12 yo	\N	\N	2018-01-20 13:33:48.972002	2018-01-20 13:33:48.972002
19	4	Teacher 50 12 yo	\N	\N	2018-01-20 13:34:20.898915	2018-01-20 13:34:20.898915
20	4	Black Dog 12 yo	\N	\N	2018-01-20 13:34:35.202799	2018-01-20 13:34:35.202799
21	4	Chivas Regal 12 yo	\N	\N	2018-01-20 13:34:54.61847	2018-01-20 13:34:54.61847
22	4	JW Black Label 12 yo	\N	\N	2018-01-20 13:35:16.947002	2018-01-20 13:35:16.947002
23	4	Vat 69	\N	\N	2018-01-20 13:35:29.59467	2018-01-20 13:35:29.59467
24	4	Ballantines	\N	\N	2018-01-20 13:35:41.810543	2018-01-20 13:35:41.810543
25	4	Black & White	\N	\N	2018-01-20 13:35:53.314386	2018-01-20 13:35:53.314386
26	5	Glenlivet 12 yo	\N	\N	2018-01-20 13:36:30.922079	2018-01-20 13:36:30.922079
27	5	Glenfiddich 12 yo	\N	\N	2018-01-20 13:36:41.818343	2018-01-20 13:36:41.818343
28	5	Talisker 10 yo	\N	\N	2018-01-20 13:37:07.146065	2018-01-20 13:37:07.146065
30	5	Glenmorangie 10 yo	\N	\N	2018-01-20 13:37:41.24978	2018-01-20 13:37:41.24978
29	5	Singleton 12 yo	\N	\N	2018-01-20 13:37:16.3539	2018-01-20 13:38:31.250499
31	5	Glenfiddich 18 yo	\N	\N	2018-01-20 13:39:07.753351	2018-01-20 13:39:07.753351
32	5	Clyelish 14 yo	\N	\N	2018-01-20 13:39:30.688685	2018-01-20 13:39:30.688685
33	5	Paul John Bold	\N	\N	2018-01-20 13:39:45.992816	2018-01-20 13:39:45.992816
34	5	Lagavulin 16 yo	\N	\N	2018-01-20 13:40:05.472693	2018-01-20 13:40:05.472693
35	5	Jura	\N	\N	2018-01-20 13:40:14.624369	2018-01-20 13:40:14.624369
36	5	Monkey Shoulder	\N	\N	2018-01-20 13:40:24.504446	2018-01-20 13:40:24.504446
37	6	Gordon's	\N	\N	2018-01-20 13:41:35.824403	2018-01-20 13:41:35.824403
38	6	Bombay Sapphire	\N	\N	2018-01-20 13:41:58.272002	2018-01-20 13:41:58.272002
39	6	Tanqueray	\N	\N	2018-01-20 13:42:11.335667	2018-01-20 13:42:11.335667
40	6	Hendricks	\N	\N	2018-01-20 13:45:24.630298	2018-01-20 13:45:24.630298
41	6	Beefeater	\N	\N	2018-01-20 13:45:39.452389	2018-01-20 13:45:39.452389
42	7	Camino Real(gold)	\N	\N	2018-01-20 13:46:33.269881	2018-01-20 13:46:33.269881
43	7	Jose Cueruo(gold)	\N	\N	2018-01-20 13:46:55.085927	2018-01-20 13:46:55.085927
44	7	XXX Tequila(silver)	\N	\N	2018-01-20 13:47:18.238016	2018-01-20 13:47:18.238016
45	7	Don Julio Blanco(silver)	\N	\N	2018-01-20 13:47:37.805127	2018-01-20 13:47:37.805127
46	8	Smirnoff	\N	\N	2018-01-20 13:48:06.188803	2018-01-20 13:48:06.188803
47	8	Absolute	\N	\N	2018-01-20 13:48:18.141333	2018-01-20 13:48:18.141333
48	8	Belvedere	\N	\N	2018-01-20 13:48:29.021107	2018-01-20 13:48:29.021107
49	8	GreyGoose	\N	\N	2018-01-20 13:48:44.181182	2018-01-20 13:48:44.181182
50	8	Finlandia	\N	\N	2018-01-20 13:48:53.836953	2018-01-20 13:48:53.836953
51	8	Ciroc	\N	\N	2018-01-20 13:49:01.436739	2018-01-20 13:49:01.436739
52	9	Henessy Vs	\N	\N	2018-01-20 13:49:36.23687	2018-01-20 13:49:36.23687
53	9	Napoleon French	\N	\N	2018-01-20 13:49:52.620412	2018-01-20 13:49:52.620412
54	10	Night NUBZ	\N	\N	2018-01-20 13:50:16.396336	2018-01-20 13:50:16.396336
55	10	Dance Reward	\N	\N	2018-01-20 13:50:27.988082	2018-01-20 13:50:27.988082
56	10	Watermelon Fizz	\N	\N	2018-01-20 13:50:38.771821	2018-01-20 13:50:38.771821
57	10	Golden Showers	\N	\N	2018-01-20 13:50:51.923742	2018-01-20 13:50:51.923742
58	10	New Fashion Drink	\N	\N	2018-01-20 13:51:05.684231	2018-01-20 13:51:05.684231
59	10	Paan Fusion	\N	\N	2018-01-20 13:51:17.923955	2018-01-20 13:51:17.923955
60	10	LIIT	\N	\N	2018-01-20 13:51:28.651654	2018-01-20 13:51:28.651654
61	10	Mojito	\N	\N	2018-01-20 13:51:37.35594	2018-01-20 13:51:37.35594
62	10	Sangria	\N	\N	2018-01-20 13:51:51.867552	2018-01-20 13:51:51.867552
63	11	Indian Flag	\N	\N	2018-01-20 13:53:54.850515	2018-01-20 13:53:54.850515
64	11	B 52	\N	\N	2018-01-20 13:54:04.794326	2018-01-20 13:54:04.794326
65	11	Mini Margarita	\N	\N	2018-01-20 13:54:19.938296	2018-01-20 13:54:19.938296
66	11	Jagerbomb	\N	\N	2018-01-20 13:54:33.290561	2018-01-20 13:54:33.290561
67	12	Vina Opera chardonnay	\N	\N	2018-01-20 13:55:03.890129	2018-01-20 13:55:03.890129
68	12	Jacobs Creek chardonnay	\N	\N	2018-01-20 13:55:15.930191	2018-01-20 13:55:15.930191
69	12	Valdivieso Sauvignon blanc	\N	\N	2018-01-20 13:55:40.474034	2018-01-20 13:55:40.474034
70	12	Fabel Sauvignon blanc	\N	\N	2018-01-20 13:55:49.857938	2018-01-20 13:55:49.857938
71	12	Vina Tarapaca chardonnay	\N	\N	2018-01-20 13:56:14.953565	2018-01-20 13:56:14.953565
72	12	Sula Chenin blanc	\N	\N	2018-01-20 13:56:32.145949	2018-01-20 13:56:32.145949
73	12	Grover Sauvignon blanc	\N	\N	2018-01-20 13:56:42.497805	2018-01-20 13:56:42.497805
74	12	Borsao Macabeo mascut	\N	\N	2018-01-20 13:57:03.889295	2018-01-20 13:57:03.889295
75	13	Danzante merlot	\N	\N	2018-01-20 13:57:53.136985	2018-01-20 13:57:53.136985
76	13	Vina Opera cabernet merlot	\N	\N	2018-01-20 13:58:10.896943	2018-01-20 13:58:10.896943
77	13	Valdivieso pinot noir	\N	\N	2018-01-20 13:58:33.552472	2018-01-20 13:58:33.552472
78	13	Sula satori merlot	\N	\N	2018-01-20 13:58:45.656564	2018-01-20 13:58:45.656564
79	13	Vina tarapaca cabernet sauvignon	\N	\N	2018-01-20 13:59:11.912667	2018-01-20 13:59:11.912667
80	13	Valdivieso merlot	\N	\N	2018-01-20 13:59:27.344156	2018-01-20 13:59:27.344156
81	13	Grover La Reserve	\N	\N	2018-01-20 13:59:42.592002	2018-01-20 13:59:42.592002
82	13	Borsao Grenache	\N	\N	2018-01-20 14:00:01.8799	2018-01-20 14:00:01.8799
83	14	Sula Zinfandel	\N	\N	2018-01-20 14:00:28.823986	2018-01-20 14:00:28.823986
84	14	Valdivieso cabernet sauvignon	\N	\N	2018-01-20 14:00:59.095806	2018-01-20 14:00:59.095806
85	14	Meteus rose	\N	\N	2018-01-20 14:01:10.807796	2018-01-20 14:01:10.807796
86	15	Moet & Chandon brut imperial	\N	\N	2018-01-20 14:01:50.031448	2018-01-20 14:01:50.031448
87	15	Valdivieso sparkling brut	\N	\N	2018-01-20 14:02:09.870893	2018-01-20 14:02:09.870893
88	19	Baileys	\N	\N	2018-01-20 14:05:28.725682	2018-01-20 14:05:28.725682
89	19	Jagermiester	\N	\N	2018-01-20 14:05:41.765467	2018-01-20 14:05:41.765467
90	19	Sambuca	\N	\N	2018-01-20 14:05:51.149464	2018-01-20 14:05:51.149464
91	19	Cointreu	\N	\N	2018-01-20 14:06:04.917323	2018-01-20 14:06:04.917323
92	19	Absinthe	\N	\N	2018-01-20 14:06:13.373345	2018-01-20 14:06:13.373345
93	20	Jamaican Passion	\N	\N	2018-01-20 14:06:37.589048	2018-01-20 14:06:37.589048
94	20	Cranberry	\N	\N	2018-01-20 14:06:47.084999	2018-01-20 14:06:47.084999
95	20	Lemonade	\N	\N	2018-01-20 14:06:57.877033	2018-01-20 14:06:57.877033
\.


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_id_seq', 95, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schema_migrations (version) FROM stdin;
20180120112125
20180120113433
\.


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: index_products_on_category_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_products_on_category_id ON products USING btree (category_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

