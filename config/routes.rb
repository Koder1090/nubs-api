Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'info/home'
  get 'info/help'
  get 'info/about'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, defaults: { format: :json } do
  	namespace :v1 do
  		resources :generate_orders do
  			collection do
  				get :alcohol_listing
          get :mixers_listing
          get :place_order
          get :fetch_orders
  			end
  		end
      
      resources :mobile_users do
        collection do
          post :register
        end
      end
  	end
  end

  root 'info#home'



end
