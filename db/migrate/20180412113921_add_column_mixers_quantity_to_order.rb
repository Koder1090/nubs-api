class AddColumnMixersQuantityToOrder < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :mixers_quantity, :integer
  end
end
