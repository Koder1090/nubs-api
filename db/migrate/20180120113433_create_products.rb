class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
    	t.belongs_to :category, index: true
    	t.string :name
    	t.integer :base_price
    	t.integer :high_price
    	t.timestamps
    end
  end
end
