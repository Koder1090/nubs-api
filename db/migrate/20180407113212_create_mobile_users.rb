class CreateMobileUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :mobile_users do |t|
      t.string :uuid, null: false
      t.string :os_type, null: false
      t.string :auth_key, null: false

      t.timestamps null: false
    end
    add_index :mobile_users, [:uuid, :auth_key], unique: true
  end
end
